package com.silverline.Cafs.Tests;

import com.silverline.Cafs.BaseTests.BaseTests;
import org.junit.Test;

import java.lang.*;

public class SmokeTests extends BaseTests{

    @Test
    public void CAFS01_CheckDataOnTotalPage() throws InterruptedException {
        System.out.println("[INFO]: Test 'CAFS01 Check Data On Total Page' STARTED >>>");
        //--->>>Navigate to ClientDetails Page
        //mm.WaitForHomePage();
        System.out.println("[INFO]: Navigate to 'Client Details' page.");
        cd.GoToClientDetailsPage();
        //--->>>Fill Form
        System.out.println("[INFO]: Fill fields on 'Client Details' page.");
        cd.FillFieldsOnClientDetailsPage();
        //--->>>Submit Form
        System.out.println("[INFO]: Submit 'Client Details' form.");
        mm.NavigateToNextPage(cd.SubmitButtonOnClientDetailsPage, cd.SolutationDataPageTwo);
        //---->>>Check Client Data on Page 2
        System.out.println("[INFO]: Check 'Client Details' data on Second page.");
        cd.CheckFieldsOnClientDetailsPage();
        //--->>>Go to Build Household
        System.out.println("[INFO]: Navigate to 'Build Household' page.");
        mm.NavigateToNextPage(cd.NextButtonOnClientDetailsPageTwo, bh.RelatedLegalEntityField);
        //--->>>Fill Build Household form
        System.out.println("[INFO]: Fill fields on 'Build Household' page.");
        bh.FillFieldsOnBuildHouseholdPage();
        //--->>>Go to Opportunity
        System.out.println("[INFO]: Navigate to 'Opportunity' page.");
        mm.NavigateToNextPage(bh.NextButtonOnBuildHouseholdPage, opp.RecordTypeField);
        //--->>>Fill Opportunity form
        System.out.println("[INFO]: Fill fields on 'Opportunity' page.");
        opp.FillFieldsOnOpportunityPage();
        //--->>>Go to Opportunity Roles
        System.out.println("[INFO]: Submit 'Opportunity' form.");
        mm.NavigateToNextPage(opp.SmallSubmitButtonOnOpportunityPage, opp.LegalEntityField);
        //--->>>Fill Opportunity Roles form
        System.out.println("[INFO]: Create new Opportunity Role.");
        opp.CreateNewOpportunityRole();
        //--->>>Navigate to Final Page
        System.out.println("[INFO]: Submit form.");
        mm.NavigateToNextPage(opp.BigSubmitButtonOnOpportunityPage, t.TotalBlock);
        //--->>>Check Data on Total Page
        System.out.println("[INFO]: Check data on 'Total' page.");
        t.CheckDataOnTotalPage();
        System.out.println("[INFO]: Test 'CAFS01 Check Data On Total Page' - PASSED <<<");
    }

    @Test
    public void CAFS02_CreateLegalEntity () throws InterruptedException {
        System.out.println("[INFO]: Test 'CAFS02 Create Legal Entity' STARTED >>>");
        //--->>>Navigate to ClientDetails Page
        System.out.println("[INFO]: Navigate to 'Client Details' page.");
        cd.GoToClientDetailsPage();
        System.out.println("[INFO]: Fill fields on 'Client Details' page.");
        cd.FillFieldsOnClientDetailsPage();
        //--->>>Submit Form
        System.out.println("[INFO]: Submit 'Client Details' form.");
        mm.NavigateToNextPage(cd.SubmitButtonOnClientDetailsPage, cd.SolutationDataPageTwo);
        //---->>>Check Client Data on Page 2
        System.out.println("[INFO]: Check 'Client Details' data on Second page.");
        cd.CheckFieldsOnClientDetailsPage();
        System.out.println("[INFO]: Navigate to 'Build Household' page.");
        mm.NavigateToNextPage(cd.NextButtonOnClientDetailsPageTwo, bh.RelatedLegalEntityField);
        System.out.println("[INFO]: Navigate to 'Opportunity' page.");
        mm.NavigateToNextPage(bh.NextButtonOnBuildHouseholdPage, opp.RecordTypeField);
        //--->>>Navigate to Final Page
        System.out.println("[INFO]: Submit form.");
        mm.NavigateToNextPage(opp.BigSubmitButtonOnOpportunityPage, t.TotalBlock);
        //--->>>Navigate to Record
        System.out.println("[INFO]: Navigate to Legal Entity record.");
        mm.NavigateToNextPage(t.NavigateToRecordButton, clientTab.ClientTab);
        mm.NavigateToNextPage(clientTab.ClientTab, clientTab.FirstNameClientTab);
        System.out.println("[INFO]: Check data on 'Client' Tab.");
        clientTab.CheckDataOnClientTab();
        System.out.println("[INFO]: Navigate to 'Contact' Tab.");
        mm.NavigateToNextPage(contactTab.ContactTab, contactTab.EmailContactTab);
        System.out.println("[INFO]: Check data on 'Contact' Tab.");
        contactTab.CheckDataOnContactTab();
        System.out.println("[INFO]: Navigate to 'Demographics' Tab.");
        mm.NavigateToNextPage2(demographicsTab.DemographicsTab, demographicsTab.BirthdateDemographicsTab);
        System.out.println("[INFO]: Check data on 'Demographics' Tab.");
        //mm.CheckText(demographicsTab.BirthdateDemographicsTab, cd.Birthdate);
        System.out.println("[INFO]: Navigate to 'Financial Profile' Tab.");
        mm.NavigateToNextPage(financialTab.FinancialTab, financialTab.TotalNetFinancialTab);
        System.out.println("[INFO]: Check data on 'Financial Profile' Tab.");
        financialTab.CheckDataOnFinancialTab();
        System.out.println("[INFO]: Test 'CAFS02 Create Legal Entity' - PASSED <<<");
    }

    @Test
    public void CAFS03_CreateLegalEntityWithAccounts () throws InterruptedException {
        System.out.println("[INFO]: Test 'CAFS03 Create Legal Entity With Accounts' STARTED >>>");
        //--->>>Navigate to ClientDetails Page
        System.out.println("[INFO]: Navigate to 'Build Household' page.");
        bh.GoToBuildHouseholdPage();
        bh.AddNewAccounts();
        System.out.println("[INFO]: Navigate to 'Opportunity' page.");
        mm.NavigateToNextPage(bh.NextButtonOnBuildHouseholdPage, opp.RecordTypeField);
        System.out.println("[INFO]: Submit form.");
        mm.NavigateToNextPage(opp.BigSubmitButtonOnOpportunityPage, t.TotalBlock);
        System.out.println("[INFO]: Check Accounts Data on Total Page.");
        t.CheckAccountsDataOnTotalPage();
        System.out.println("[INFO]: Navigate to Legal Entity record.");
        t.TotalPageOpenBlockOneButton.click();
        mm.NavigateToNextPage(t.NavigateToRecordButton, clientTab.FirstNameClientTab);
        //mm.NavigateToNextPage(clientTab.ClientTab, clientTab.FirstNameClientTab);
        System.out.println("[INFO]: Check Relationships on 'Client' Tab.");
        clientTab.CheckRelationshipDataOnClientTab();
        System.out.println("[INFO]: Test 'CAFS03 Create Legal Entity With Accounts' - PASSED <<<");
    }
}
