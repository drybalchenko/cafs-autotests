package com.silverline.Cafs.BaseTests;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.concurrent.TimeUnit;

import com.silverline.Cafs.Helpers.BrowsersHelper;
import com.silverline.Cafs.Methods.MainMethods;
import com.silverline.Cafs.Pages.*;
import com.silverline.Cafs.Pages.PersonLegalEntity.*;
import com.silverline.Cafs.Pages.PersonLegalEntity.SuitabilityTab;
import org.junit.AfterClass;
import org.junit.BeforeClass;

import org.openqa.selenium.WebDriver;

import org.openqa.selenium.chrome.ChromeDriver;

public class BaseTests {

    public static WebDriver driver;
    public static ClientDetails cd;
    public static BuildHousehold bh;
    public static Opportunity opp;
    public static MainMethods mm;
    public static Total t;
    public static ClientTab clientTab;
    public static ContactTab contactTab;
    public static DemographicsTab demographicsTab;
    public static DetailsTab detailsTab;
    public static FinancialTab financialTab;
    public static SuitabilityTab suitabilityTab;

    @BeforeClass
    public static void setup() {
        //System.setProperty("webdriver.chrome.driver", "D:\\2018_!!!\\comsilverlineCafs\\chromedriver.exe");
        //driver = new ChromeDriver();
        //driver = BrowsersHelper.getChromeLocalWebDriver();
    	driver = BrowsersHelper.getChromeRemoteWebDriver();
        cd = new ClientDetails(driver);
        bh = new BuildHousehold(driver);
        opp = new Opportunity(driver);
        mm = new MainMethods(driver);
        t = new Total(driver);
        clientTab = new ClientTab(driver);
        contactTab = new ContactTab(driver);
        demographicsTab = new DemographicsTab(driver);
        detailsTab = new DetailsTab(driver);
        financialTab = new FinancialTab(driver);
        suitabilityTab = new SuitabilityTab(driver);
        driver.manage().window().maximize();
        //driver.manage().timeouts().implicitlyWait(2, TimeUnit.MINUTES);
        /*
        try {
            System.out.println(InetAddress.getLocalHost());
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        */
        driver.get("https://test.salesforce.com/?un=dmitry.rybalchenko@silverlinecrm.com18&pw=Autotest@2");
        //driver.get("https://test.salesforce.com/?un=silverline.cacu@silverlinecrm.com.cafs.test&pw=Silverline18");
        mm.WaitForHomePage();
    }

    @AfterClass
    public static void end() {
        driver.quit();
    }
}
