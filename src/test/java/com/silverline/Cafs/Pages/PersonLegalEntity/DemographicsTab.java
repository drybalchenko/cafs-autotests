package com.silverline.Cafs.Pages.PersonLegalEntity;

import com.silverline.Cafs.BaseTests.BaseTests;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class DemographicsTab  extends BaseTests {

    public WebDriver driver;

    public DemographicsTab(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    @FindBy(css = ".tabHeader[data-tab-name^='Demographics-']")
    public WebElement DemographicsTab;

    @FindBy(css = ".uiTab:nth-child(4) .slds-wrap:nth-child(1) .slds-size_1-of-2:nth-child(1) .uiOutputText")
    public WebElement MaritalStatusDemographicsTab;

    @FindBy(css = ".uiTab:nth-child(4) .slds-wrap:nth-child(1) .slds-size_1-of-2:nth-child(2) .uiOutputDate")
    public WebElement BirthdateDemographicsTab;

    @FindBy(css = ".uiTab:nth-child(4) .slds-wrap:nth-child(2) .slds-size_1-of-2:nth-child(1) .uiOutputText")
    public WebElement GenderDemographicsTab;

    @FindBy(css = ".uiTab:nth-child(4) .slds-wrap:nth-child(2) .slds-size_1-of-2:nth-child(2) .uiOutputText")
    public WebElement AgeDemographicsTab;

    @FindBy(css = ".uiTab:nth-child(4) .slds-wrap:nth-child(3) .slds-size_1-of-2:nth-child(1) .uiOutputText")
    public WebElement NumberOfDemographicsTab;

    @FindBy(css = ".uiTab:nth-child(4) .slds-wrap:nth-child(3) .slds-size_1-of-2:nth-child(2) .uiOutputText")
    public WebElement BirthCountryDemographicsTab;

    @FindBy(css = ".uiTab:nth-child(4) .slds-wrap:nth-child(4) .slds-size_1-of-2:nth-child(1) .uiOutputText")
    public WebElement CitizenshipCountryDemographicsTab;
}
