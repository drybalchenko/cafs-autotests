package com.silverline.Cafs.Pages.PersonLegalEntity;

import com.silverline.Cafs.BaseTests.BaseTests;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class SuitabilityTab  extends BaseTests {

    public WebDriver driver;

    public SuitabilityTab(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }
}
