package com.silverline.Cafs.Pages.PersonLegalEntity;

import com.silverline.Cafs.BaseTests.BaseTests;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ContactTab  extends BaseTests {

    public WebDriver driver;

    public ContactTab(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    @FindBy(css = ".tabHeader[data-tab-name^='Contact-']")
    public WebElement ContactTab;

    @FindBy(css = ".flexipageComponent .cSL_FL_cmp_Row .uiOutputEmail")
    public WebElement EmailContactTab;

    @FindBy(css = ".uiTab:nth-child(3) .slds-wrap:nth-child(1) .slds-size_1-of-2:nth-child(2) .uiOutputText")
    public WebElement WorkPhoneContactTab;

    @FindBy(css = ".uiTab:nth-child(3) .slds-wrap:nth-child(2) .slds-size_1-of-2:nth-child(1) .uiOutputText")
    public WebElement HomePhoneContactTab;

    @FindBy(css = ".uiTab:nth-child(3) .slds-wrap:nth-child(2) .slds-size_1-of-2:nth-child(2) .uiOutputText")
    public WebElement MobileContactTab;

    @FindBy(css = ".uiTab:nth-child(3) .slds-wrap:nth-child(3) .slds-size_1-of-2:nth-child(1) .uiOutputText")
    public WebElement HomeStreetContactTab;

    @FindBy(css = ".uiTab:nth-child(3) .slds-wrap:nth-child(3) .slds-size_1-of-2:nth-child(2) .uiOutputText")
    public WebElement MailimgStreetContactTab;

    @FindBy(css = ".uiTab:nth-child(3) .slds-wrap:nth-child(4) .slds-size_1-of-2:nth-child(1) .uiOutputText")
    public WebElement HomeCityContactTab;

    @FindBy(css = ".uiTab:nth-child(3) .slds-wrap:nth-child(4) .slds-size_1-of-2:nth-child(2) .uiOutputText")
    public WebElement MailingCityContactTab;

    @FindBy(css = ".uiTab:nth-child(3) .slds-wrap:nth-child(5) .slds-size_1-of-2:nth-child(1) .uiOutputText")
    public WebElement HomeStateContactTab;

    @FindBy(css = ".uiTab:nth-child(3) .slds-wrap:nth-child(5) .slds-size_1-of-2:nth-child(2) .uiOutputText")
    public WebElement MailingStateContactTab;

    @FindBy(css = ".uiTab:nth-child(3) .slds-wrap:nth-child(6) .slds-size_1-of-2:nth-child(1) .uiOutputText")
    public WebElement HomeZipContactTab;

    @FindBy(css = ".uiTab:nth-child(3) .slds-wrap:nth-child(6) .slds-size_1-of-2:nth-child(2) .uiOutputText")
    public WebElement MailingZipContactTab;

    @FindBy(css = ".uiTab:nth-child(3) .slds-wrap:nth-child(7) .slds-size_1-of-2:nth-child(1) .uiOutputText")
    public WebElement HomeCountryContactTab;

    @FindBy(css = ".uiTab:nth-child(3) .slds-wrap:nth-child(7) .slds-size_1-of-2:nth-child(2) .uiOutputText")
    public WebElement MailingCountryContactTab;

    //---METHODS
    public void CheckDataOnContactTab(){
        mm.CheckText(EmailContactTab, cd.Email);
        mm.CheckText(HomePhoneContactTab, cd.HomePhone);
        //mm.CheckText(HomeStreetContactTab, cd.HomeStreet);
        mm.CheckText(HomeCityContactTab, cd.HomeCity);
        mm.CheckText(HomeStateContactTab, cd.HomeState);
        mm.CheckText(HomeZipContactTab, cd.HomeZip);
        mm.CheckText(HomeCountryContactTab, cd.HomeCountryFull);
    }
}
