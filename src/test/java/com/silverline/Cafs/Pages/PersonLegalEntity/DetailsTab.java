package com.silverline.Cafs.Pages.PersonLegalEntity;

import com.silverline.Cafs.BaseTests.BaseTests;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class DetailsTab  extends BaseTests {

    public WebDriver driver;

    public DetailsTab(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    @FindBy(css = ".tabHeader[title='Details']")
    public WebElement DetailsTab;

    @FindBy(css = ".forcePageBlockSectionView:nth-child(1) .forcePageBlockSectionRow:nth-child(1) .forcePageBlockItemView:nth-child(1) .itemBody")
    public WebElement TotalNetFinancialTab;

    @FindBy(css = ".uiTab:nth-child(5) .slds-wrap:nth-child(1) .slds-size_1-of-2:nth-child(2) .itemBody")
    public WebElement iquidNetFinancialTab;

    @FindBy(css = ".uiTab:nth-child(5) .slds-wrap:nth-child(2) .slds-size_1-of-2:nth-child(1) .itemBody")
    public WebElement AnnualIncomeFinancialTab;

    @FindBy(css = ".uiTab:nth-child(5) .slds-wrap:nth-child(2) .slds-size_1-of-2:nth-child(2) .itemBody")
    public WebElement TaxBracketFinancialTab;

    @FindBy(css = ".uiTab:nth-child(5) .slds-wrap:nth-child(3) .slds-size_1-of-2:nth-child(1) .itemBody")
    public WebElement RetirementDateFinancialTab;

    @FindBy(css = ".uiTab:nth-child(5) .slds-wrap:nth-child(3) .slds-size_1-of-2:nth-child(2) .itemBody")
    public WebElement TotalLifeFinancialTab;
}
