package com.silverline.Cafs.Pages.PersonLegalEntity;

import com.silverline.Cafs.BaseTests.BaseTests;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ClientTab  extends BaseTests {

    public WebDriver driver;

    public ClientTab(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    @FindBy(css = ".tabHeader[data-tab-name^='Client-']")
    public WebElement ClientTab;

    @FindBy(css = ".uiTab:nth-child(2) .slds-card .slds-wrap:nth-child(1) .slds-size_1-of-2:nth-child(1) .uiOutputText")
    public WebElement SalutationClientTab;

    @FindBy(css = ".uiTab:nth-child(2) .slds-card .slds-wrap:nth-child(1) .slds-size_1-of-2:nth-child(2) .uiOutputText")
    public WebElement FirstNameClientTab;

    @FindBy(css = ".uiTab:nth-child(2) .slds-card .slds-wrap:nth-child(2) .slds-size_1-of-2:nth-child(1) .uiOutputText")
    public WebElement MiddleNameClientTab;

    @FindBy(css = ".uiTab:nth-child(2) .slds-card .slds-wrap:nth-child(2) .slds-size_1-of-2:nth-child(2) .uiOutputText")
    public WebElement LastNameClientTab;

    @FindBy(css = ".uiTab:nth-child(3) .slds-card .slds-wrap:nth-child(3) .slds-size_1-of-2:nth-child(1) .uiOutputText")
    public WebElement SuffixClientTab;

    @FindBy(css = ".uiTab:nth-child(3) .slds-card .slds-wrap:nth-child(3) .slds-size_1-of-2:nth-child(2) .uiOutputText")
    public WebElement LegalNameClientTab;

    @FindBy(css = ".uiTab:nth-child(2) .slds-card .slds-wrap:nth-child(4) .slds-size_1-of-2:nth-child(1) .uiOutputText")
    public WebElement LegalEntityOwnerClientTab;

    @FindBy(css = ".uiTab:nth-child(2) .slds-card .slds-wrap:nth-child(4) .slds-size_1-of-2:nth-child(2) .uiOutputText")
    public WebElement FaTeamClientTab;

    @FindBy(css = ".uiTab:nth-child(2) .slds-card .slds-wrap:nth-child(5) .slds-size_1-of-2:nth-child(1) .uiOutputText")
    public WebElement HouseholdClientTab;

    @FindBy(css = ".uiTab:nth-child(2) .slds-card .slds-wrap:nth-child(5) .slds-size_1-of-2:nth-child(2) .uiOutputText")
    public WebElement LastReviewDateClientTab;

    @FindBy(css = ".uiTab:nth-child(2) .slds-card .slds-wrap:nth-child(6) .slds-size_1-of-2:nth-child(1) .uiOutputText")
    public WebElement NextReviewDateClientTab;

    //---Households
    @FindBy(css = ".forceBrandBand .forcePageHost:nth-child(3) .slds-truncate[title='Create Household']")
    public WebElement CreateHouseholdButtonOnClientTab;

    @FindBy(css = ".flexipageBaseRecordHomeTemplateDesktop .flexipageComponent:nth-child(8) .forceOutputLookupWithPreview")
    public WebElement HouseholdMemberOnClientTab;

    @FindBy(css = "//a[@class='outputLookupLink slds-truncate forceOutputLookup'][1]")
    public WebElement HouseholdNameOnClientTab;

    @FindBy(css = "//a[@class='outputLookupLink slds-truncate forceOutputLookup'][2]")
    public WebElement HouseholdRecordTypeOnClientTab;

    //---Relationships
    @FindBy(xpath = "//tbody/tr[1]/td[1]")
    public WebElement FirstRelationshipOnClientTab;

    @FindBy(xpath = "//tbody/tr[1]/td[2]")
    public WebElement FirstLegalEntityOnClientTab;

    @FindBy(xpath = "//tbody/tr[2]/td[1]")
    public WebElement SecondRelationshipOnClientTab;

    @FindBy(xpath = "//tbody/tr[2]/td[2]")
    public WebElement SecondLegalEntityOnClientTab;

    //---Tabs on the botton
    @FindBy(css = ".tabHeader[title='Activity']")
    public WebElement ActivityTabOnClientTab;

    //---METHODS
    public void CheckDataOnClientTab(){
        mm.CheckText(SalutationClientTab, cd.Solutation);
        mm.CheckText(FirstNameClientTab, cd.FirstName);
        mm.CheckText(LastNameClientTab, cd.LastName+cd.LN2);
        mm.CheckText(LegalEntityOwnerClientTab, cd.LegalEntityOwner);
        mm.CheckText(HouseholdClientTab, cd.Household);
    }

    public void CheckRelationshipDataOnClientTab(){
        ActivityTabOnClientTab.sendKeys(Keys.ENTER);
        mm.CheckText(FirstRelationshipOnClientTab, cd.TestRelationship1);
        mm.CheckText(FirstLegalEntityOnClientTab, cd.TestUser1);
        mm.CheckText(SecondRelationshipOnClientTab, cd.TestRelationship2);
        mm.CheckText(SecondLegalEntityOnClientTab, cd.TestUser2);
    }
}
