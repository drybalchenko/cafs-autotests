package com.silverline.Cafs.Pages.PersonLegalEntity;

import com.silverline.Cafs.BaseTests.BaseTests;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class FinancialTab  extends BaseTests {

    public WebDriver driver;

    public FinancialTab(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    @FindBy(css = ".tabHeader[data-tab-name^='Financial Profile-']")
    public WebElement FinancialTab;

    @FindBy(css = ".uiTab:nth-child(5) .slds-wrap:nth-child(1) .slds-size_1-of-2:nth-child(1) .uiOutputText")
    public WebElement TotalNetFinancialTab;

    @FindBy(css = ".uiTab:nth-child(5) .slds-wrap:nth-child(1) .slds-size_1-of-2:nth-child(2) .uiOutputText")
    public WebElement LiquidNetFinancialTab;

    @FindBy(css = ".uiTab:nth-child(5) .slds-wrap:nth-child(2) .slds-size_1-of-2:nth-child(1) .uiOutputText")
    public WebElement AnnualIncomeFinancialTab;

    @FindBy(css = ".uiTab:nth-child(5) .slds-wrap:nth-child(2) .slds-size_1-of-2:nth-child(2) .uiOutputText")
    public WebElement TaxBracketFinancialTab;

    @FindBy(css = ".uiTab:nth-child(5) .slds-wrap:nth-child(3) .slds-size_1-of-2:nth-child(1) .uiOutputDate")
    public WebElement RetirementDateFinancialTab;

    @FindBy(css = ".uiTab:nth-child(5) .slds-wrap:nth-child(3) .slds-size_1-of-2:nth-child(2) .uiOutputCurrency")
    public WebElement TotalLifeFinancialTab;

    public void CheckDataOnFinancialTab() throws InterruptedException {
        Thread.sleep(3000);
        //mm.CheckText(TotalNetFinancialTab, cd.TotalNetWorthFull);
        mm.CheckText(LiquidNetFinancialTab, cd.LiquidNetWorthFull);
        mm.CheckText(AnnualIncomeFinancialTab, cd.AnnualIncomeFull);
        mm.CheckText(TaxBracketFinancialTab, cd.TaxBracketFull);
        mm.CheckText(RetirementDateFinancialTab, "May 10, 2000");
        mm.CheckText(TotalLifeFinancialTab, cd.TotalLifeInsuranceFull);
    }
}
