package com.silverline.Cafs.Pages;

import com.silverline.Cafs.BaseTests.BaseTests;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class BuildHousehold extends BaseTests{

    public WebDriver driver;

    public BuildHousehold(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    public static String ErrorMessageForRelatedLegalEntity = "Required fields are missing: [Related Legal Entity]";

    //---FIELDS
    @FindBy(xpath ="//form[@id='form-wrapper']/div[@class='slds-grid slds-wrap']/div[1]/div//input[@type='text']")
    public WebElement RelatedLegalEntityField;

    @FindBy(css =".slds-listbox__item.cSL_cmp_InputLookupElement")
    public WebElement RelatedLegalEntityFirstValue;

    @FindBy(xpath ="/html//form[@id='form-wrapper']/div/div[2]/div/select")
    public WebElement RelationshipPicklist;

    @FindBy(xpath ="//label[@for ='input-20']/span[@class='slds-checkbox_faux']")
    public WebElement IsImmediateFamilyCheckbox;

    @FindBy(xpath ="//label[@for ='input-21']/span[@class='slds-checkbox_faux']")
    public WebElement IsDependentCheckbox;

    @FindBy(xpath ="//label[@for ='input-22']/span[@class='slds-checkbox_faux']")
    public WebElement IsBeneficiaryCheckbox;

    //---Add New Account
    @FindBy(css ="//li[@class='slds-listbox__item' and @role='presentation']")
    public WebElement AddNewAccountButtonOnBuildHouseholdPage;

    @FindBy(xpath =".slds-listbox__option-text_entity")
    public WebElement AddNewAccountButton;

    @FindBy(xpath ="//div[@id='modal-content-id-1']/form[@id='form-wrapper']/div[@class='slds-grid slds-wrap']/div[2]/lightning-input//input[@type='text']")
    public WebElement AddNewAccountFirstName;

    @FindBy(xpath ="//div[@id='modal-content-id-1']/form[@id='form-wrapper']/div[@class='slds-grid slds-wrap']/div[3]/lightning-input//input[@type='text']")
    public WebElement AddNewAccountLastName;

    @FindBy(css ="[id=\"modal-content-id-1\"] .slds-m-vertical--medium")
    public WebElement AddNewAccountSubmitButton;

    @FindBy(xpath ="//div[@id='modal-content-id-1']")
    public WebElement AddNewAccountWindow;

    @FindBy(css =".slds-accordion  .slds-accordion__list-item:nth-child(1)  .slds-accordion__content")
    public WebElement FirstAccountBlock;

    @FindBy(css =".slds-accordion  .slds-accordion__list-item:nth-child(2)  .slds-accordion__content")
    public WebElement SecondAccountBlock;

    @FindBy(css =".slds-accordion__list-item:nth-child(1)  .slds-accordion__summary-action-icon.slds-button_icon.slds-button_icon-left.slds-icon.slds-icon-text-default.slds-icon_x-small")
    public WebElement OpenFirstAccountBlock;

    @FindBy(css =".slds-accordion__list-item:nth-child(2)  .slds-accordion__summary-action-icon.slds-button_icon.slds-button_icon-left.slds-icon.slds-icon-text-default.slds-icon_x-small")
    public WebElement OpenSecondAccountBlock;

    //---BUTTONS
    @FindBy(css =".cSL_cmp_AccordionElement:nth-child(2) .SL_cmp_AccordionElementBody .cSL_cmp_DynamicForm .slds-m-vertical--medium")
    public WebElement SubmitButtonOnBuildHouseholdPage;

    @FindBy(xpath ="//div[@id='brandBand_1']/div/div[1]//div[@class='cSL_cmp_AccordionForm']/div/div[2]/div[@class='SL_cmp_AccordionElementFooter']/button[1]")
    public WebElement PreviousButtonOnBuildHouseholdPage;

    @FindBy(css =".cSL_cmp_AccordionElement:nth-child(2) .SL_cmp_AccordionElementFooter .SL_cmp_AccordionElementButton-next")
    public WebElement NextButtonOnBuildHouseholdPage;

    //---METHODS
    public void GoToBuildHouseholdPage(){
        cd.GoToClientDetailsPage();
        mm.FillField(cd.LastNameField, "TestName"+Math.random());
        mm.NavigateToNextPage(cd.SubmitButtonOnClientDetailsPage, cd.SolutationDataPageTwo);
        mm.NavigateToNextPage(cd.NextButtonOnClientDetailsPageTwo, RelatedLegalEntityField);
    }

    public void FillFieldsOnBuildHouseholdPage() throws InterruptedException {
        mm.SelectFromSearchField(RelatedLegalEntityField, RelatedLegalEntityFirstValue, cd.FirstName+" "+cd.LastName+cd.LN2);
        mm.SelectFromPicklist(RelationshipPicklist, 2);
        mm.ClickOn(IsImmediateFamilyCheckbox);
        mm.ClickOn(IsDependentCheckbox);
        mm.ClickOn(IsBeneficiaryCheckbox);
    }

    public void SelectAccount(String name, int value) throws InterruptedException {
        mm.SelectFromSearchField(RelatedLegalEntityField, RelatedLegalEntityFirstValue, name);
        mm.SelectFromPicklist(RelationshipPicklist, value);
        mm.ClickOn(SubmitButtonOnBuildHouseholdPage);
    }

    public void AddNewAccounts() throws InterruptedException {
        SelectAccount(cd.TestUser1, 4);
        OpenFirstAccountBlock.isDisplayed();
        SelectAccount(cd.TestUser2, 3);
        OpenSecondAccountBlock.isDisplayed();
    }

    public void CreateNewAccountOnBuildHouseholdPage() throws InterruptedException {
        mm.ClickOn(RelatedLegalEntityField);
        mm.ClickOn(AddNewAccountButtonOnBuildHouseholdPage);
    }
}
